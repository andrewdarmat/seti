var slider = $('.js-slider');

if (slider.length) {
  slider.slick({
    infinite: false,
    arrows: false,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true
  });
}
